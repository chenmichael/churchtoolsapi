using System.ComponentModel.DataAnnotations;
using System.Text.Json;
using Cnml.ChurchToolsApi.Errors;
using Google.Authenticator;
using Microsoft.Extensions.Configuration;

namespace Cnml.ChurchToolsApi.Tests;

[TestClass]
public class ChurchToolsApiClientTests {
    private const string _cookieFile = "cookie.txt";
    private static readonly IConfiguration _configuration = new ConfigurationBuilder()
        .AddInMemoryCollection([
            new($"{ApiConnectionInfo.Name}:Host", "kirche-barmstedt.krz.tools"),
            new($"{ApiConnectionInfo.Name}:Username", "michael@theyoungrevolution.de"),
        ])
        .AddEnvironmentVariables()
        .AddUserSecrets<ChurchToolsApiClientTests>()
        .Build();

    private static TConfig GetConfiguration<TConfig>(string sectionName) {
        var config = _configuration
            .GetSection(sectionName)
            .Get<TConfig>()
            ?? throw new InvalidProgramException($"Configuration '{sectionName}' is missing!");
        Validator.ValidateObject(config, new ValidationContext(config), true);
        return config;
    }

    private readonly ApiConnectionInfo _connectionInfo = GetConfiguration<ApiConnectionInfo>(ApiConnectionInfo.Name);
    private static string? _loadedCookie;
    private static string? _loginCookie;

    public class ApiConnectionInfo {
        public const string Name = "ChurchTools";
        [Required] public required string Host { get; set; }
        [Required] public required string Username { get; set; }
        [Required] public required string Password { get; set; }
        public string? TotpSecret { get; set; }
        private static readonly TwoFactorAuthenticator _tfa = new();
        public string? TotpCode => TotpSecret is { } secret ? _tfa.GetCurrentPIN(secret, true) : null;
    }

    [ClassInitialize]
    public static async Task Initialize(TestContext context) {
        try {
            _loadedCookie = _loginCookie = await File.ReadAllTextAsync(_cookieFile);
        } catch (FileNotFoundException) {
            _loginCookie = null;
        }
    }

    [ClassCleanup]
    public static async Task Cleanup() {
        if (_loginCookie is { } cookie && cookie != _loadedCookie)
            await File.WriteAllTextAsync(_cookieFile, cookie);
    }

    private ChurchToolsApiClient GetFreshClient() => new(_connectionInfo.Host);
    private async Task<ChurchToolsApiClient> GetAuthenticatedClient() {
        var cookie = _loginCookie ??= await GetFreshCookie();
        var client = GetFreshClient();
        client.SetCookie(cookie);
        return client;
    }

    private async Task<string> GetFreshCookie() {
        var client = GetFreshClient();
        await client.LoginWithOtpAsync(new() {
            Username = _connectionInfo.Username,
            Password = _connectionInfo.Password,
            RememberMe = true,
            Code = _connectionInfo.TotpCode,
        });
        var cookie = client.GetCookie();
        await Console.Error.WriteLineAsync($"Requested new cookie: {_loginCookie}");
        return cookie;
    }

    [TestMethod]
    public async Task GetConfigAsyncTest() {
        var _client = await GetAuthenticatedClient();
        var result = await _client.GetConfigAsync();
        Assert.AreEqual("Kirche Barmstedt", result.ShortName);
    }

    [TestMethod]
    public async Task GetInfoAsyncTest() {
        var client = GetFreshClient();
        var result = await client.GetInfoAsync();
        Assert.AreEqual("Kirche Barmstedt", result.ShortName);
    }

    [TestMethod]
    public async Task LoginAsyncTestInvalidCredentials() {
        var client = GetFreshClient();
        var exception = await Assert.ThrowsExceptionAsync<ApiException>(
            async () => await client.LoginAsync(new() {
                Password = "somePassword",
                Username = "unknownUser",
            }));
        Assert.IsInstanceOfType<LoginFailedError>(exception.Error);
        await Console.Error.WriteLineAsync(exception.ToString());
    }

    [TestMethod]
    public async Task GetSelfAsync() {
        var client = await GetAuthenticatedClient();
        var person = await client.GetSelfAsync();
        Assert.AreEqual("Michael", person.FirstName);
        Assert.AreEqual("Chen", person.LastName);
        Assert.AreEqual(19, person.Id);
        Assert.AreEqual(new DateTimeOffset(2023, 5, 5, 12, 53, 55, TimeSpan.Zero), person.Meta.CreatedDate);
        Assert.AreEqual(_connectionInfo.Username, person.Email);
    }
}
