using System.Net.Http.Json;

namespace Cnml.ChurchToolsApi;
/// <summary>
/// Provides extension methods for the <see cref="HttpContent"/>.
/// </summary>
internal static class HttpContentExtensions {
    /// <summary>
    /// Reads the JSON document from the <paramref name="content"/> and validates it as required.
    /// </summary>
    /// <typeparam name="T">Type schema of the JSON document.</typeparam>
    /// <param name="content">Content to load the JSON from.</param>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>Parsed non-null JSON content.</returns>
    /// <exception cref="FormatException">JSON content is a null object.</exception>
    public static async Task<T> ReadFromJsonNotNullAsync<T>(this HttpContent content, CancellationToken cancellationToken)
        where T : notnull
        => await content.ReadFromJsonAsync<T>(cancellationToken)
        ?? throw new FormatException("JSON response was null!");
}
