using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <inheritdoc cref="ListResponse{TData, TClient}"/>
public class ListResponseUnattached<TData>
    : MetaDataResponseUnattached<IReadOnlyList<TData>
    , ListResponseUnattached<TData>.Metadata> {
    /// <summary>
    /// List request metadata.
    /// </summary>
    public class Metadata {
        /// <summary>
        /// Number of items in the list.
        /// </summary>
        [JsonPropertyName("count")] public required int Count { get; set; }
    }
}

/// <summary>
/// Provides a data wrapper class for listing responses.
/// </summary>
/// <typeparam name="TData">Type of the listed data.</typeparam>
/// <typeparam name="TClient">Client type to attach to the list data.</typeparam>
public class ListResponse<TData, TClient>
    : ListResponseUnattached<TData>
    , IClientAttachable<TClient>
    where TData : IClientAttachable<TClient> {
    void IClientAttachable<TClient>.AttachClient(TClient client) {
        foreach (var item in Data)
            item.AttachClient(client);
    }
}
