namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Structure that can be used to attach a linkable client to a class.
/// </summary>
/// <param name="client">Optionally provide a default value to attach to the object.</param>
/// <typeparam name="TClient">Type of the client that is attached.</typeparam>
/// <exception cref="InvalidProgramException">Client is accessed, but it was not attached previously.</exception>
public struct ClientLink<TClient>(TClient? client = null)
    : IClientLinked<TClient>
    where TClient : class {
    private TClient? _client = client;
    /// <inheritdoc/>
    public readonly TClient GetClient() => _client ?? throw new InvalidProgramException("Client was not previously attached to this attachable!");
    /// <inheritdoc/>
    public void AttachClient(TClient client) => _client = client;
}
