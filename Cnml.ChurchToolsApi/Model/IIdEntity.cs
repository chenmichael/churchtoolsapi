namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Interface for entities of type <typeparamref name="TEntity"/> that contain a unique ID in their namespace.
/// </summary>
/// <typeparam name="TEntity">Type of the represented entity.</typeparam>
public interface IIdEntity<TEntity>
    where TEntity : IIdEntity<TEntity> {
    /// <summary>
    /// Unique ID that represents this <typeparamref name="TEntity"/> in its namespace.
    /// </summary>
    int Id { get; }
}
