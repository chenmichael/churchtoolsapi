namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Provides a concrete implementation base for <see cref="PersonEntity"/> like objects.
/// </summary>
public abstract class PersonEntity : ApiEntity<PersonEntity, ChurchToolsApiClient>;
