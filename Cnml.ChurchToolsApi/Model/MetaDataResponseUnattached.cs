using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Wraps an object into a data layer with additional metadata information.
/// </summary>
/// <typeparam name="TData">Type of the data content.</typeparam>
/// <typeparam name="TMetadata">Type of the associated metadata.</typeparam>
public class MetaDataResponseUnattached<TData, TMetadata>
    : DataResponseUnattached<TData> {
    /// <summary>
    /// Metadata associated with the given response.
    /// </summary>
    [JsonPropertyName("meta")] public required TMetadata Meta { get; set; }
}
