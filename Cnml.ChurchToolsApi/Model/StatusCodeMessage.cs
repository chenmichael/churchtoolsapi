using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Simple status code string wrapper.
/// </summary>
public class StatusCodeMessage {
    /// <summary>
    /// Status string value.
    /// </summary>
    [JsonPropertyName("status")] public required string Status { get; set; }
}
