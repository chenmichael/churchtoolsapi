using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Class that contains detailed information about a person.
/// </summary>
public class PersonData : PersonEntity {
    /// <inheritdoc/>
    [JsonIgnore] public override int Id => PersonId;
    /// <inheritdoc cref="Id"/>
    [JsonPropertyName("id")] public required int PersonId { get; set; }
    /// <summary>
    /// Guid that uniquely identifies this user.
    /// </summary>
    [JsonPropertyName("guid")] public required Guid Guid { get; set; }
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    [JsonPropertyName("securityLevelForPerson")] public required int SecurityLevelForPerson { get; set; }
    [JsonPropertyName("editSecurityLevelForPerson")] public required int EditSecurityLevelForPerson { get; set; }
    [JsonPropertyName("title")] public required string Title { get; set; }
    /// <summary>
    /// Gets the first name of the user.
    /// </summary>
    [JsonPropertyName("firstName")] public required string FirstName { get; set; }
    /// <summary>
    /// Gets the last name of the user.
    /// </summary>
    [JsonPropertyName("lastName")] public required string LastName { get; set; }
    [JsonPropertyName("nickname")] public required string Nickname { get; set; }
    [JsonPropertyName("job")] public required string Job { get; set; }
    [JsonPropertyName("street")] public required string Street { get; set; }
    [JsonPropertyName("addressAddition")] public required string AddressAddition { get; set; }
    [JsonPropertyName("zip")] public required string Zip { get; set; }
    [JsonPropertyName("city")] public required string City { get; set; }
    [JsonPropertyName("country")] public required string Country { get; set; }
    [JsonPropertyName("latitude")] public required double Latitude { get; set; }
    [JsonPropertyName("longitude")] public required double Longitude { get; set; }
    [JsonPropertyName("latitudeLoose")] public required float LatitudeLoose { get; set; }
    [JsonPropertyName("longitudeLoose")] public required float LongitudeLoose { get; set; }
    [JsonPropertyName("phonePrivate")] public required string PhonePrivate { get; set; }
    [JsonPropertyName("phoneWork")] public required string PhoneWork { get; set; }
    [JsonPropertyName("mobile")] public required string Mobile { get; set; }
    [JsonPropertyName("birthday")] public required DateOnly Birthday { get; set; }
    [JsonPropertyName("imageUrl")] public required string ImageUrl { get; set; }
    [JsonPropertyName("familyImageUrl")] public required string FamilyImageUrl { get; set; }
    [JsonPropertyName("email")] public required string Email { get; set; }
    [JsonPropertyName("emails")] public required IReadOnlyList<Email> Emails { get; set; }
    [JsonPropertyName("privacyPolicyAgreement")] public required RootElementPrivacyPolicyAgreement PrivacyPolicyAgreement { get; set; }
    [JsonPropertyName("privacyPolicyAgreementDate")] public required DateOnly PrivacyPolicyAgreementDate { get; set; }
    [JsonPropertyName("privacyPolicyAgreementTypeId")] public required int PrivacyPolicyAgreementTypeId { get; set; }
    [JsonPropertyName("privacyPolicyAgreementWhoId")] public required int PrivacyPolicyAgreementWhoId { get; set; }
    [JsonPropertyName("nationalityId")] public required int NationalityId { get; set; }
    [JsonPropertyName("departmentIds")] public required IReadOnlyList<int> DepartmentIds { get; set; }
    [JsonPropertyName("growPathId")] public required int GrowPathId { get; set; }
    [JsonPropertyName("canChat")] public required bool CanChat { get; set; }
    [JsonPropertyName("invitationStatus")] public required string InvitationStatus { get; set; }
    [JsonPropertyName("chatActive")] public required bool ChatActive { get; set; }
    [JsonPropertyName("isArchived")] public required bool IsArchived { get; set; }
    [JsonPropertyName("meta")] public required RootElementMeta Meta { get; set; }
    [JsonPropertyName("acceptedsecurity")] public required DateOnly AcceptedSecurity { get; set; }
    [JsonPropertyName("schwimmabzeichen")] public required string Schwimmabzeichen { get; set; }
    [JsonPropertyName("schwimmerlaubnis")] public required bool Schwimmerlaubnis { get; set; }
    [JsonPropertyName("krankenkasse")] public required string Krankenkasse { get; set; }
    [JsonPropertyName("name_eltern")] public required string NameEltern { get; set; }
    [JsonPropertyName("tel_eltern")] public required string TelEltern { get; set; }
    [JsonPropertyName("allergien")] public required string Allergien { get; set; }
}

public class Email {
    [JsonPropertyName("email")] public required string Address { get; set; }
    [JsonPropertyName("isDefault")] public required bool IsDefault { get; set; }
    [JsonPropertyName("contactLabelId")] public required int ContactLabelId { get; set; }
}

public class RootElementPrivacyPolicyAgreement {
    [JsonPropertyName("date")] public required DateOnly Date { get; set; }
    [JsonPropertyName("typeId")] public required int TypeId { get; set; }
    [JsonPropertyName("whoId")] public required int WhoId { get; set; }
}

public class RootElementMeta {
    [JsonPropertyName("createdDate")] public required DateTimeOffset CreatedDate { get; set; }
    [JsonPropertyName("createdPerson")] public required PersonIdEntity CreatedPerson { get; set; }
    [JsonPropertyName("modifiedDate")] public required DateTimeOffset ModifiedDate { get; set; }
    [JsonPropertyName("modifiedPerson")] public required PersonIdEntity ModifiedPerson { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}

/// <summary>
/// Concrete person entity with a specific id.
/// </summary>
public class PersonIdEntity : PersonEntity {
    /// <inheritdoc/>
    [JsonIgnore] public override int Id => PersonId;
    /// <inheritdoc cref="Id"/>
    [JsonPropertyName("id")] public int PersonId { get; set; }
}
