using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Basic server information object.
/// </summary>
public class ServerInfo {
    /// <summary>
    /// Build number of the server software.
    /// </summary>
    [JsonPropertyName("build")] public required string Build { get; set; }
    /// <summary>
    /// Short name of the server instance.
    /// </summary>
    [JsonPropertyName("shortName")] public required string ShortName { get; set; }
    /// <summary>
    /// Full name of the server instance.
    /// </summary>
    [JsonPropertyName("siteName")] public required string SiteName { get; set; }
    /// <summary>
    /// Version that the server runs.
    /// </summary>
    [JsonPropertyName("version")] public required string Version { get; set; }
}
