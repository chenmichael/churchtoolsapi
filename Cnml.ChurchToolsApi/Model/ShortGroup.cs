using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Group membership information.
/// </summary>
public class ShortGroup : GroupEntity {
    /// <inheritdoc/>
    public override int Id => DomainIdentifier;
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    [JsonPropertyName("domainIdentifier")] public required int DomainIdentifier { get; set; }
    [JsonPropertyName("title")] public required string Title { get; set; }
    [JsonPropertyName("domainType")] public required string DomainType { get; set; }
    [JsonPropertyName("apiUrl")] public required string ApiUrl { get; set; }
    [JsonPropertyName("frontendUrl")] public required string FrontendUrl { get; set; }
    [JsonPropertyName("imageUrl")] public required string ImageUrl { get; set; }
    [JsonPropertyName("icon")] public required string Icon { get; set; }
    [JsonPropertyName("domainAttributes")] public required RootElementDataGroupDomainAttributes DomainAttributes { get; set; }
}

public class RootElementDataGroupDomainAttributes {
    [JsonPropertyName("note")] public required string Note { get; set; }
    [JsonPropertyName("groupStatusId")] public required int GroupStatusId { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}
