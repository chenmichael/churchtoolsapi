namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Type that provides JSON supporatable entity base clases with optional client linking.
/// </summary>
/// <typeparam name="TEntity">Type of the entity group to represent.</typeparam>
/// <typeparam name="TClient">Type of the API client.</typeparam>
public abstract class ApiEntity<TEntity, TClient>
    : IApiEntity<TEntity, TClient>
    , IClientAttachable<TClient>
    where TEntity : IIdEntity<TEntity>
    where TClient : class {
    /// <inheritdoc/>
    public abstract int Id { get; }
    private readonly ClientLink<TClient> _clientLink = new();
    TClient IAttachedClient<TClient>.GetClient() => _clientLink.GetClient();
    void IClientAttachable<TClient>.AttachClient(TClient client) => _clientLink.AttachClient(client);
}
