using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Log in to the server using plain credential authentication.
/// </summary>
public class LoginRequest {
    /// <summary>
    /// Password to authorize the user with.
    /// </summary>
    [JsonPropertyName("password")] public required string Password { get; set; }
    /// <summary>
    /// Whether to remember the login session for longer.
    /// Usually 1 week instead of 1 day if set to <see langword="true"/>.
    /// </summary>
    [JsonPropertyName("rememberMe")] public bool RememberMe { get; set; } = false;
    /// <summary>
    /// Login user, can be the username or email.
    /// </summary>
    [JsonPropertyName("username")] public required string Username { get; set; }
}
