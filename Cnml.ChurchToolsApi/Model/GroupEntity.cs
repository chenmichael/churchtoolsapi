namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Provides a concrete implementation base for <see cref="GroupEntity"/> like objects.
/// </summary>
public abstract class GroupEntity : ApiEntity<GroupEntity, ChurchToolsApiClient>;
