using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Class that contains information about the group membership.
/// </summary>
public class GroupMembership
    : PersonEntity
    , IClientAttachable<ChurchToolsApiClient> {
    /// <inheritdoc/>
    [JsonIgnore] public override int Id => PersonId;
    /// <inheritdoc cref="Id"/>
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    [JsonPropertyName("personId")] public required int PersonId { get; set; }
    [JsonPropertyName("person")] public required PersonData Person { get; set; }
    [JsonPropertyName("group")] public required ShortGroup Group { get; set; }
    [JsonPropertyName("groupTypeRoleId")] public required int GroupTypeRoleId { get; set; }
    [JsonPropertyName("groupMemberStatus")] public required string GroupMemberStatus { get; set; }
    [JsonPropertyName("memberStartDate")] public required DateOnly MemberStartDate { get; set; }
    [JsonPropertyName("followUpStep")] public object? FollowUpStep { get; set; }
    [JsonPropertyName("followUpDiffDays")] public object? FollowUpDiffDays { get; set; }
    [JsonPropertyName("registeredBy")] public object? RegisteredBy { get; set; }
    [JsonPropertyName("followUpUnsuccessfulBackGroupId")] public object? FollowUpUnsuccessfulBackGroupId { get; set; }
    [JsonPropertyName("fields")] public required IReadOnlyList<object> Fields { get; set; }
    [JsonPropertyName("personFields")] public required IReadOnlyList<object> PersonFields { get; set; }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    /// <inheritdoc/>
    public void AttachClient(ChurchToolsApiClient client) {
        ((IClientAttachable<ChurchToolsApiClient>)Person).AttachClient(client);
        ((IClientAttachable<ChurchToolsApiClient>)Group).AttachClient(client);
    }
}
