namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Enumeration of group member status values.
/// </summary>
public enum GroupMemberStatus {
    /// <summary>
    /// Group membership is currently active.
    /// </summary>
    Active,
}
