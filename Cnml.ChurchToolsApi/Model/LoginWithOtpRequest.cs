namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Provides a combined login request that allows for automatically calling the 2FA once the login occurred.
/// </summary>
public class LoginWithOtpRequest {
    /// <inheritdoc cref="LoginTotpRequest.Code"/>
    public required string? Code { get; set; }
    /// <inheritdoc cref="LoginRequest.Password"/>
    public required string Password { get; set; }
    /// <inheritdoc cref="LoginRequest.RememberMe"/>
    public bool RememberMe { get; set; } = false;
    /// <inheritdoc cref="LoginRequest.Username"/>
    public required string Username { get; set; }
    /// <summary>
    /// Gets the corresponding plain login request.
    /// </summary>
    /// <returns>Plain login request.</returns>
    public LoginRequest GetLoginRequest() => new() {
        Password = Password,
        Username = Username,
        RememberMe = RememberMe
    };
    /// <summary>
    /// Gets the TOTP login request given the <paramref name="personId"/> (received from the <see cref="LoginResponse.PersonId"/>).
    /// </summary>
    /// <param name="personId">Person ID to authorize.</param>
    /// <returns>TOTP login request for the given person.</returns>
    public LoginTotpRequest? GetTotpRequest(int personId) => Code is { } code ? new() {
        PersonId = personId,
        Code = code,
    } : null;
}
