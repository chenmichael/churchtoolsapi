using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Response to a plain login request.
/// </summary>
public class LoginResponse : PersonEntity {
    /// <inheritdoc/>
    [JsonIgnore] public override int Id => PersonId;
    /// <summary>
    /// Location to redirect the user to.
    /// </summary>
    [JsonPropertyName("location")] public string? Location { get; set; }
    /// <summary>
    /// Login status message.
    /// </summary>
    [JsonPropertyName("message")] public required string Message { get; set; }
    /// <inheritdoc cref="Id"/>
    [JsonPropertyName("personId")] public required int PersonId { get; set; }
    /// <summary>
    /// Status of the login. If the status is <c>"totp"</c> an additional TOTP authentication is required to authorize the session.
    /// </summary>
    [JsonPropertyName("status")] public required string Status { get; set; }
}
