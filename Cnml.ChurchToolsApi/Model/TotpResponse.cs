using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Response to a TOTP login request.
/// </summary>
public class TotpResponse : StatusCodeMessage {
    /// <inheritdoc cref="LoginResponse.Location"/>
    [JsonPropertyName("redirectTo")] public required string RedirectTo { get; set; }
}
