using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <summary>
/// Login OTP request, that can be done successfully exactly once after logging in with the correct credentials.
/// </summary>
public class LoginTotpRequest {
    /// <summary>
    /// Provides the TOTP code for the 2FA login.
    /// </summary>
    [JsonPropertyName("code")] public required string Code { get; set; }
    /// <summary>
    /// Unique user ID of the user that is trying to authenticate.
    /// </summary>
    [JsonPropertyName("personId")] public required int PersonId { get; set; }
}
