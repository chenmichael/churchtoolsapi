using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Model;
/// <inheritdoc cref="DataResponse{TData, TClient}"/>
public class DataResponseUnattached<TData> {
    /// <summary>
    /// Data content that is wrapped.
    /// </summary>
    [JsonPropertyName("data")] public required TData Data { get; set; }
}

/// <summary>
/// Wraps an object into a data layer.
/// </summary>
/// <typeparam name="TData">Type of the data content.</typeparam>
/// <typeparam name="TClient">Client that can be attached to the data.</typeparam>
public class DataResponse<TData, TClient>
    : DataResponseUnattached<TData>
    , IClientAttachable<TClient>
    where TData : IClientAttachable<TClient> {
    void IClientAttachable<TClient>.AttachClient(TClient client)
        => Data.AttachClient(client);
}
