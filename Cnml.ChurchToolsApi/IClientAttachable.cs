namespace Cnml.ChurchToolsApi;
/// <summary>
/// Interface for entities that one can attach and retrieve a linked client object for chained API calls.
/// </summary>
/// <typeparam name="TClient"></typeparam>
public interface IClientLinked<TClient>
    : IClientAttachable<TClient>
    , IAttachedClient<TClient>;

/// <summary>
/// Interface for classes that a <typeparamref name="TClient"/> can be attached onto for chained API calling.
/// </summary>
/// <typeparam name="TClient">Type of the client to attach.</typeparam>
public interface IClientAttachable<TClient> {
    /// <summary>
    /// Attach the <paramref name="client"/> to the object.
    /// </summary>
    /// <param name="client">Client to attach to the object.</param>
    void AttachClient(TClient client);
}

/// <summary>
/// Interface for classes that contain a <typeparamref name="TClient"/> for chained API calling.
/// </summary>
/// <typeparam name="TClient">Type of the client attached to the object.</typeparam>
public interface IAttachedClient<TClient> {
    /// <summary>
    /// Gets the attached client object.
    /// </summary>
    /// <returns>Client object attached to the object.</returns>
    TClient GetClient();
}
