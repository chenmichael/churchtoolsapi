using Cnml.ChurchToolsApi.Model;

namespace Cnml.ChurchToolsApi;
/// <summary>
/// Interface for entity hubs that can create single entity endpoints.
/// </summary>
/// <typeparam name="TEntity">Type of the entity.</typeparam>
/// <typeparam name="TSelf">Type of the entity hub.</typeparam>
[Obsolete($"Not using hubs anymore! Replaced by {nameof(IApiEntity<TEntity, ChurchToolsApiClient>)} extension methods!")]
public interface IEntityApi<TEntity, TSelf>
    where TEntity : IIdEntity<TEntity>
    where TSelf : IEntityApi<TEntity, TSelf> {
    /// <summary>
    /// Create the entity API from the given <paramref name="id"/>.
    /// </summary>
    /// <param name="id">Entity ID to create the API for.</param>
    /// <returns>Entity API for the current <paramref name="id"/>.</returns>
    static abstract TSelf Create(int id);
    /// <summary>
    /// Create the entity API from the given <paramref name="entity"/>.
    /// </summary>
    /// <param name="entity">Entity to create the API for.</param>
    /// <returns>Entity API for the current <paramref name="entity"/>.</returns>
    static TSelf Create(TEntity entity) => TSelf.Create(entity.Id);
}
