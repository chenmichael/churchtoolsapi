using Cnml.ChurchToolsApi.Model;

namespace Cnml.ChurchToolsApi;
/// <summary>
/// Extension methods that provide the <see cref="PersonEntity"/> API to the client by using API entities.
/// This allows for chaining of different types of requests.
/// </summary>
public static class PersonEntityExtensions {
    /// <summary>
    /// Gets all child groups of the current <paramref name="person"/>.
    /// </summary>
    /// <param name="person">Person resource to query.</param>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>List of all group memberships.</returns>
    public static async Task<IReadOnlyList<GroupMembership>> GetGroupsAsync(this IApiEntity<PersonEntity, ChurchToolsApiClient> person, CancellationToken cancellationToken = default)
        => (await person.GetClient().GetJsonAsync<ListResponse<GroupMembership, ChurchToolsApiClient>>($"persons/{person.Id}/groups", cancellationToken)).Data;
}
