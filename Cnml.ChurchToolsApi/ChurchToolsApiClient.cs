using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Net.Mime;
using Cnml.ChurchToolsApi.Errors;
using Cnml.ChurchToolsApi.Model;

namespace Cnml.ChurchToolsApi;
/// <summary>
/// This class provides the API client to interact with the ChurchTools server.
/// </summary>
public class ChurchToolsApiClient {
    private readonly CookieContainer _cookieContainer = new();
    private readonly Uri _baseAddress;
    private readonly HttpClient _client;
    /// <summary>
    /// The hostname of the ChurchTools server to interact with.
    /// </summary>
    public string Host { get; }
    /// <summary>
    /// Create a new API client that connects to the specified server.
    /// </summary>
    /// <param name="host">The hostname of the server to interact with.</param>
    /// <param name="scheme">The scheme that the server uses.</param>
    /// <param name="port">The TCP port that the server listens on.</param>
    public ChurchToolsApiClient(string host, string scheme = "https", int port = 443) {
        var handler = new HttpClientHandler() {
            UseCookies = true,
            CookieContainer = _cookieContainer,
        };
        _baseAddress = new UriBuilder(scheme, host, port, "/api/").Uri;
        _client = new(handler, true) {
            BaseAddress = _baseAddress,
        };
        _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json));
        Host = host;
    }

    #region General - Endpoints of general purpose
    /// <summary>
    /// Get the ChurchTools config.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>ChurchTools server configuration.</returns>
    public async Task<ServerConfig> GetConfigAsync(CancellationToken cancellationToken = default)
    => await GetJsonAsync<ServerConfig>("config", cancellationToken);
    /// <summary>
    /// Gts information about public API.
    /// This method does not require authentication.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>ChurchTools server information.</returns>
    public async Task<ServerInfo> GetInfoAsync(CancellationToken cancellationToken = default)
        => await GetJsonAsync<ServerInfo>("info", cancellationToken);
    /// <summary>
    /// Login with username and password.
    /// If the login response status is "totp", a <see cref="LoginOtpAsync(LoginTotpRequest, CancellationToken)"/> is necessary to authorize.
    /// </summary>
    /// <param name="request">Login request data.</param>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>Login response.</returns>
    public async Task<LoginResponse> LoginAsync(LoginRequest request, CancellationToken cancellationToken = default)
        => (await PostJsonAsync<LoginRequest, DataResponse<LoginResponse, ChurchToolsApiClient>>("login", request, cancellationToken)).Data;
    /// <summary>
    /// Gets the currently logged in user.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>User data of the logged in user.</returns>
    public async Task<PersonData> GetSelfAsync(CancellationToken cancellationToken = default)
        => (await GetJsonAsync<DataResponse<PersonData, ChurchToolsApiClient>>("whoami?only_allow_authenticated=true", cancellationToken)).Data;
    /// <summary>
    /// Tries to get the currently logged in user.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>User data of the logged in user.</returns>
    public async Task<PersonData?> TryGetSelfAsync(CancellationToken cancellationToken = default) {
        try {
            return (await GetJsonAsync<DataResponse<PersonData, ChurchToolsApiClient>>("whoami?only_allow_authenticated=true", cancellationToken)).Data;
        } catch (ApiException e) when (e.Error is UnauthorizedError) {
            return null;
        }
    }
    /// <summary>
    /// Login the given user and, if necessary, present the OTP code to the server to complete 2FA login.
    /// </summary>
    /// <param name="request">Login credentials.</param>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>If OTP was required, the OTP response, else <see langword="null"/>.</returns>
    /// <exception cref="ApiException">OTP code is required, but was not provided.</exception>
    public async Task<TotpResponse?> LoginWithOtpAsync(LoginWithOtpRequest request, CancellationToken cancellationToken = default) {
        var login = await LoginAsync(request.GetLoginRequest(), cancellationToken);
        if (login.Status is not "totp") return null;
        var otpRequest = request.GetTotpRequest(login.PersonId)
            ?? throw new ApiException(new UnknownError {
                Message = "Login requires OTP, but an OTP code was not provided!",
            });
        return await LoginOtpAsync(otpRequest, cancellationToken);
    }

    /// <summary>
    /// Presents the OTP code to the server to complete a previously started <see cref="LoginAsync(LoginRequest, CancellationToken)"/>.
    /// </summary>
    /// <param name="request">TOTP login credentials.</param>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>The OTP response object.</returns>
    public async Task<TotpResponse> LoginOtpAsync(LoginTotpRequest request, CancellationToken cancellationToken = default)
        => (await PostJsonAsync<LoginTotpRequest, DataResponseUnattached<TotpResponse>>("login/totp", request, cancellationToken)).Data;
    #endregion

    #region Reference Entities
    /// <summary>
    /// Gets the person with the given ID.
    /// </summary>
    /// <param name="id">ID of the person to get.</param>
    /// <returns>Person reference entity.</returns>
    public ReferenceEntity<PersonEntity, ChurchToolsApiClient> Person(int id) => new(id, this);
    #endregion

    #region Request Helpers
    /// <summary>
    /// Gets the resource at the given path from ChurchTools and returns the parsed response.
    /// </summary>
    /// <typeparam name="TResponse">Expected response schema.</typeparam>
    /// <param name="path">Path to the API resource.</param>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>Parsed JSON response.</returns>
    /// <exception cref="ApiException">Error object if the request failed.</exception>
    internal async Task<TResponse> GetJsonAsync<TResponse>([StringSyntax(StringSyntaxAttribute.Uri)] string path, CancellationToken cancellationToken)
        where TResponse : notnull
        => await HandleResponseAsync<TResponse>(await _client.GetAsync(path, cancellationToken), cancellationToken);
    /// <summary>
    /// Posts the <paramref name="content"/> to the resource at the given path from ChurchTools and returns the parsed response.
    /// </summary>
    /// <typeparam name="TRequest">Request body schema.</typeparam>
    /// <typeparam name="TResponse">Expected response schema.</typeparam>
    /// <param name="path">Path to the API resource.</param>
    /// <param name="content">Content body to post to the resource as JSON.</param>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>Parsed JSON response.</returns>
    /// <exception cref="ApiException">Error object if the request failed.</exception>
    internal async Task<TResponse> PostJsonAsync<TRequest, TResponse>([StringSyntax(StringSyntaxAttribute.Uri)] string path, TRequest content, CancellationToken cancellationToken)
        where TResponse : notnull
        => await HandleResponseAsync<TResponse>(await _client.PostAsJsonAsync(path, content, cancellationToken), cancellationToken);
    /// <summary>
    /// Handle the response from ChurchTools and parse (error) responses.
    /// </summary>
    /// <typeparam name="T">Expected response schema.</typeparam>
    /// <param name="response">Response object.</param>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>Parsed JSON response.</returns>
    /// <exception cref="ApiException">Error object if the request failed.</exception>
    private async Task<T> HandleResponseAsync<T>(HttpResponseMessage response, CancellationToken cancellationToken)
        where T : notnull {
        var content = response.Content;
        if (!response.IsSuccessStatusCode) {
            var error = await content.ReadFromJsonNotNullAsync<ErrorBase>(cancellationToken);
            throw new ApiException(error);
        }
        var result = await content.ReadFromJsonNotNullAsync<T>(cancellationToken);
        if (result is IClientAttachable<ChurchToolsApiClient> attachable) attachable.AttachClient(this);
#if DEBUG
        else await Console.Error.WriteLineAsync($"Warning: Entity of type '{result.GetType().FullName}' without attachment returned!");
#endif
        return result;
    }

    /// <summary>
    /// Login using the provided cookie.
    /// </summary>
    /// <param name="cookieValue">Cookie value.</param>
    /// <param name="cookieName">Name of the cookie (server specific), or null to attempt to automatically infer it.</param>
    public void CookieLogin(string cookieValue, string? cookieName = null)
        => CookieLogin(new Cookie(cookieName ?? GetCookieName(), cookieValue, null, Host));
    /// <summary>
    /// Login using the provided cookie.
    /// </summary>
    /// <param name="cookie">Cookie value.</param>
    public void CookieLogin(Cookie cookie)
        => _cookieContainer.Add(cookie);
    /// <summary>
    /// Sets the login cookie from the given header string.
    /// </summary>
    /// <param name="cookieHeader">Login cookie header.</param>
    public void SetCookie(string cookieHeader)
        => _cookieContainer.SetCookies(_baseAddress, cookieHeader);
    /// <summary>
    /// Gets the login cookie header string from the current client session.
    /// </summary>
    /// <returns>Login cookie header.</returns>
    public string GetCookie()
        => _cookieContainer.GetCookieHeader(_baseAddress);
    /// <summary>
    /// Gets or sets the login cookie header currently in use by the client session.
    /// </summary>
    public string LoginCookie { get => GetCookie(); set => SetCookie(value); }

    private string GetCookieName() {
        var tld = Host.LastIndexOf('.', 0, Host.Length);
        var mld = Host.LastIndexOf('.', 0, tld);
        if (mld is < 0) throw new InvalidProgramException("Cookie name cannot be determined from host!");
        var name = $"ChurchTools_ct_{Host[..mld]}";
        Console.Error.WriteLine($"Determined cookie name as: {name}");
        return name;
    }
    #endregion
}
