using Cnml.ChurchToolsApi.Model;

namespace Cnml.ChurchToolsApi;
/// <summary>
/// Extension methods that provide the <see cref="GroupEntity"/> API to the client by using API entities.
/// This allows for chaining of different types of requests.
/// </summary>
public static class GroupEntityExtensions {
    /// <summary>
    /// Gets all child groups of the current <paramref name="group"/>.
    /// </summary>
    /// <param name="group">Group resource to query.</param>
    /// <param name="cancellationToken">Cancellation token to abort the request.</param>
    /// <returns>List of all child groups.</returns>
    public static async Task<IReadOnlyList<GroupMembership>> GetChildrenAsync(this IApiEntity<GroupEntity, ChurchToolsApiClient> group, CancellationToken cancellationToken = default)
        => (await group.GetClient().GetJsonAsync<ListResponse<GroupMembership, ChurchToolsApiClient>>($"groups/{group.Id}/children", cancellationToken)).Data;
}
