using Cnml.ChurchToolsApi.Model;

namespace Cnml.ChurchToolsApi;
/// <summary>
/// Interface that is applied to all entities of the same group.
/// Could be different representations of a single <typeparamref name="TEntity"/>.
/// Attached client can be retrieved for chained calls.
/// </summary>
/// <typeparam name="TEntity">Type of the entity group.</typeparam>
/// <typeparam name="TClient">Type of the attached client.</typeparam>
public interface IApiEntity<TEntity, TClient>
    : IAttachedClient<TClient>
    , IIdEntity<TEntity>
    where TEntity : IIdEntity<TEntity>;
