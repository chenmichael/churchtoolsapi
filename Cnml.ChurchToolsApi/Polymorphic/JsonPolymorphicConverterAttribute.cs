using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Polymorphic;
/// <summary>
/// Instructs the JsonSerialization to serialize this type polymorphically.
/// Annotate the <typeparamref name="TBase"/> type with <see cref="JsonPolymorphicSubtypeAttribute{TBase, TDiscriminator, TDerived}"/> and <see cref="JsonPolymorphicFallbackAttribute{TBase, TDiscriminator, TDerived}"/> to modify serialization behaviour.
/// </summary>
/// <typeparam name="TBase">Base type to serialize.</typeparam>
/// <typeparam name="TDiscriminator">Type of the discriminator value.</typeparam>
public class JsonPolymorphicConverterAttribute<TBase, TDiscriminator>()
    : JsonConverterAttribute(typeof(PolymorphicJsonConverter<TBase, TDiscriminator>))
    where TBase : class, IPolymorphicBaseClass<TBase, TDiscriminator>
    where TDiscriminator : IEquatable<TDiscriminator>;
