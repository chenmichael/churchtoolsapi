namespace Cnml.ChurchToolsApi.Polymorphic;
/// <summary>
/// Interface applied to classes that can be discriminated.
/// </summary>
/// <typeparam name="TDiscriminator">Type of the discriminator value.</typeparam>
public interface IDiscriminator<TDiscriminator>
    where TDiscriminator : IEquatable<TDiscriminator> {
    /// <summary>
    /// Gets the value of the type discriminator for this type.
    /// </summary>
    static abstract TDiscriminator Discriminator { get; }
}
