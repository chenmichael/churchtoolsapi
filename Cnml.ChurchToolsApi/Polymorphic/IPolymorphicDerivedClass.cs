namespace Cnml.ChurchToolsApi.Polymorphic;
/// <summary>
/// Interface applied to classes that are derived from the given base <typeparamref name="TBase"/>.
/// </summary>
/// <typeparam name="TBase">Type of the base class.</typeparam>
/// <typeparam name="TDiscriminator">Type of the discriminator value.</typeparam>
public interface IPolymorphicDerivedClass<TBase, TDiscriminator>
    : IDiscriminator<TDiscriminator>
    where TBase : class, IPolymorphicBaseClass<TBase, TDiscriminator>
    where TDiscriminator : IEquatable<TDiscriminator>;
