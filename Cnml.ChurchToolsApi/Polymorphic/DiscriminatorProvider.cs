using System.Reflection;

namespace Cnml.ChurchToolsApi.Polymorphic;
/// <summary>
/// Static class that provides the discriminator of different discriminator types types.
/// </summary>
/// <typeparam name="TDiscriminator"></typeparam>
public static class DiscriminatorProvider<TDiscriminator>
    where TDiscriminator : IEquatable<TDiscriminator> {
    private static MethodInfo GetDiscriminatorFunction { get; } = typeof(DiscriminatorProvider<TDiscriminator>)
        .GetMethod(nameof(GetDiscriminatorGeneric), BindingFlags.Static | BindingFlags.Public | BindingFlags.InvokeMethod)
        ?? throw new InvalidProgramException($"Could no find {nameof(GetDiscriminatorGeneric)} function!");
    /// <summary>
    /// Gets the discriminator associated with the given type.
    /// </summary>
    /// <param name="type">Type to get the discriminator for.</param>
    /// <returns>Discriminator value of the given type.</returns>
    /// <exception cref="InvalidProgramException">Type does not have a discriminator or it cannot be retrieved.</exception>
    public static TDiscriminator GetDiscriminator(Type type) {
        var member = GetDiscriminatorFunction.MakeGenericMethod(type);
        if (!type.IsAssignableTo(typeof(IDiscriminator<TDiscriminator>)))
            throw new InvalidProgramException("Type is not a discriminator type!");
        if (member.Invoke(null, []) is not TDiscriminator disc)
            throw new InvalidProgramException("Function did not return correct type!");
        return disc;
    }
    /// <summary>
    /// Gets the discriminator associated with the given <typeparamref name="TDerived"/> type.
    /// </summary>
    /// <typeparam name="TDerived">Type to get the discriminator for.</typeparam>
    /// <returns>Discriminator value of the given type.</returns>
    public static TDiscriminator GetDiscriminatorGeneric<TDerived>()
        where TDerived : IDiscriminator<TDiscriminator>
        => TDerived.Discriminator;
}
