namespace Cnml.ChurchToolsApi.Polymorphic;
/// <summary>
/// Interface applied to the base class used for polymorphic serialization.
/// This interface provides the converter with the necessary information to
/// discriminate subtypes of the base <typeparamref name="TSelf"/>.
/// </summary>
/// <typeparam name="TSelf">Type of the base class.</typeparam>
/// <typeparam name="TDiscriminator">Type of the discriminator value.</typeparam>
public interface IPolymorphicBaseClass<TSelf, TDiscriminator>
    where TSelf : class, IPolymorphicBaseClass<TSelf, TDiscriminator>
    where TDiscriminator : IEquatable<TDiscriminator> {
    /// <summary>
    /// Name of the property that holds the discriminator of type <typeparamref name="TDiscriminator"/>.
    /// </summary>
    static abstract string DiscriminatorPropertyName { get; }
}
