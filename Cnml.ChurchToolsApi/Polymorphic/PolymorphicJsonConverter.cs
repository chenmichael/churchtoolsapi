using System.Reflection;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Polymorphic;
internal sealed class PolymorphicJsonConverter<TBase, TDiscriminator>
    : JsonConverter<TBase>
    where TBase : class, IPolymorphicBaseClass<TBase, TDiscriminator>
    where TDiscriminator : IEquatable<TDiscriminator> {
    private static Dictionary<TDiscriminator, Type> DerivedTypes { get; } = typeof(TBase)
        .GetCustomAttributes(typeof(JsonPolymorphicSubtypeAttribute<,,>))
        .Select(DerivedType)
        .OfType<Type>()
        .ToDictionary(DiscriminatorProvider<TDiscriminator>.GetDiscriminator);
    private static Type? FallbackType { get; } = DerivedType(typeof(TBase).GetCustomAttribute(typeof(JsonPolymorphicFallbackAttribute<,,>)));
    private record DerivedTypeInfo(TDiscriminator Discriminator, Type Type);
    private static Type? DerivedType(Attribute? attribute) {
        if (attribute is null) return null;
        var arguments = attribute.GetType().GenericTypeArguments;
        if (arguments is not [var tBase, var tDiscriminator, var tDerived]
            || tBase != typeof(TBase)
            || tDiscriminator != typeof(TDiscriminator))
            return null;
        return tDerived;
    }
    public override TBase? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
        if (JsonNode.Parse(ref reader) is not JsonObject obj)
            throw new JsonException("Reader did not read JSON object!");
        if (!obj.TryGetPropertyValue(TBase.DiscriminatorPropertyName, out var node))
            throw new JsonException("JSON object does not contain a type discriminator!");
        if (node is not JsonValue nodeValue)
            throw new JsonException("JSON type discriminator is not a value type!");
        if (!nodeValue.TryGetValue<TDiscriminator>(out var discriminatorValue))
            throw new JsonException("Cannot read discriminator from JSON value node!");
        if (!DerivedTypes.TryGetValue(discriminatorValue, out var returnType))
            returnType = FallbackType ?? throw new JsonException($"Unknown type discriminator '{discriminatorValue}' and no fallback was specified!");
        if (obj.Deserialize(returnType, options) is not { } parsedObj)
            return null;
        if (parsedObj is not TBase parsed)
            throw new JsonException($"Parsed value cannot be assigned to the base type!");
        return parsed;
    }
    public override void Write(Utf8JsonWriter writer, TBase value, JsonSerializerOptions options) {
        var inputType = value.GetType();
        if (JsonSerializer.SerializeToNode(value, inputType, options) is not JsonObject node)
            throw new JsonException("Serializer did not write JSON object!");
        var discriminator = DiscriminatorProvider<TDiscriminator>.GetDiscriminator(inputType);
        if (JsonSerializer.SerializeToNode(discriminator, options) is not JsonValue discriminatorNode)
            throw new JsonException("Serializer did not write discriminator JSON value!");
        node.Add(TBase.DiscriminatorPropertyName, discriminatorNode);
        node.WriteTo(writer);
    }
}
