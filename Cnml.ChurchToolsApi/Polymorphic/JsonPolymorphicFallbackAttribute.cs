using System.Runtime.CompilerServices;

namespace Cnml.ChurchToolsApi.Polymorphic;
/// <summary>
/// Attribute applied to the base class specified by <typeparamref name="TBase"/> to signals the
/// converter to fall back to the derived type <typeparamref name="TDerived"/> if a discriminator
/// value for which no subtype is registered.
/// </summary>
/// <typeparam name="TBase">Type of the base class.</typeparam>
/// <typeparam name="TDiscriminator">Type of the discriminator value.</typeparam>
/// <typeparam name="TDerived">Type of the derived fallback class.</typeparam>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
public class JsonPolymorphicFallbackAttribute<TBase, TDiscriminator, TDerived>
    : Attribute
    where TBase : class, IPolymorphicBaseClass<TBase, TDiscriminator>
    where TDiscriminator : IEquatable<TDiscriminator>
    where TDerived : TBase, IPolymorphicDerivedClass<TBase, TDiscriminator>;
