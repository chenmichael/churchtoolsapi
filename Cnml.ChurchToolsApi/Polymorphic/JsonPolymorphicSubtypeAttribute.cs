namespace Cnml.ChurchToolsApi.Polymorphic;
/// <summary>
/// Attribute applied to the base class specified by <typeparamref name="TBase"/> to signals the
/// converter to polymorphically serialize the derived type <typeparamref name="TDerived"/>
/// using the given <typeparamref name="TDiscriminator"/> type.
/// </summary>
/// <typeparam name="TBase">Type of the base class.</typeparam>
/// <typeparam name="TDiscriminator">Type of the discriminator value.</typeparam>
/// <typeparam name="TDerived">Type of the derived class.</typeparam>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class JsonPolymorphicSubtypeAttribute<TBase, TDiscriminator, TDerived>
    : Attribute
    where TBase : class, IPolymorphicBaseClass<TBase, TDiscriminator>
    where TDiscriminator : IEquatable<TDiscriminator>
    where TDerived : TBase, IPolymorphicDerivedClass<TBase, TDiscriminator>;
