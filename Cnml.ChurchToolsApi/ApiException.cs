using System.Text.Json;
using Cnml.ChurchToolsApi.Errors;

namespace Cnml.ChurchToolsApi;
/// <summary>
/// Exception occurrs when a call to the API server fails. The resulting abstract <paramref name="error"/> is saved
/// in the <see cref="Error"/> field.
/// </summary>
/// <param name="error">Error that occurred in the API call.</param>
public class ApiException(ErrorBase error) : Exception(error.Message) {
    /// <summary>
    /// Error that occurred in the API call.
    /// </summary>
    public ErrorBase Error { get; } = error;
    /// <summary>
    /// Serializes the error details to a JSON string.
    /// </summary>
    /// <returns>Indented JSON string containing the error details.</returns>
    public override string ToString() => JsonSerializer.Serialize(Error, Indented);
    private static JsonSerializerOptions Indented { get; } = new() {
        WriteIndented = true,
    };
}
