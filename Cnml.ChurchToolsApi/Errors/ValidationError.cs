using System.Text.Json.Serialization;
using Cnml.ChurchToolsApi.Polymorphic;

namespace Cnml.ChurchToolsApi.Errors;
/// <summary>
/// Error that occurrs when one or many fields failed input verification.
/// </summary>
public class ValidationError : ErrorBase, IPolymorphicDerivedClass<ErrorBase, string> {
    /// <inheritdoc/>
    public static string Discriminator => "validation.error";
    /// <summary>
    /// Errors that occurred during falidation.
    /// </summary>
    [JsonPropertyName("errors")] public required IReadOnlyCollection<ErrorBase> Errors { get; set; }
}
