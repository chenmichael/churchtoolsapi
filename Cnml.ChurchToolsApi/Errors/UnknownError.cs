namespace Cnml.ChurchToolsApi.Errors;
/// <summary>
/// Fallback error object for unknown error messages.
/// </summary>
public class UnknownError : ErrorBase;
