using System.Text.Json.Serialization;

namespace Cnml.ChurchToolsApi.Errors;
/// <summary>
/// Error that occurred during validation of a single input field.
/// </summary>
public abstract class FieldValidationError : UnknownError {
    /// <summary>
    /// Name of the field for which validation failed.
    /// </summary>
    [JsonPropertyName("fieldId")] public required string FieldName { get; set; }
    /// <inheritdoc cref="Arguments"/>
    [JsonPropertyName("args")] public required Arguments Args { get; set; }
    /// <summary>
    /// Validation failure arguments.
    /// </summary>
    public class Arguments {
        /// <summary>
        /// If provided, contains the original invalidated input string.
        /// </summary>
        [JsonPropertyName("input")] public string? Input { get; set; }
    }
}
