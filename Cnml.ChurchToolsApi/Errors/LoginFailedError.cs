using Cnml.ChurchToolsApi.Polymorphic;

namespace Cnml.ChurchToolsApi.Errors;
/// <summary>
/// Error that represents a failed login request.
/// </summary>
public class LoginFailedError : ErrorBase, IPolymorphicDerivedClass<ErrorBase, string> {
    /// <inheritdoc/>
    public static string Discriminator => "login.failed";
}
