using System.Text.Json.Serialization;
using Cnml.ChurchToolsApi.Polymorphic;

namespace Cnml.ChurchToolsApi.Errors;
/// <summary>
/// Provides an abstract error class for all API error types.
/// </summary>
[JsonPolymorphicConverter<ErrorBase, string>]
[JsonPolymorphicSubtype<ErrorBase, string, LoginFailedError>]
[JsonPolymorphicSubtype<ErrorBase, string, TwoFactorCodeError>]
[JsonPolymorphicSubtype<ErrorBase, string, ValidationError>]
[JsonPolymorphicSubtype<ErrorBase, string, UnauthorizedError>]
//[JsonPolymorphicFallback<ErrorBase, string, UnknownError>]
public abstract class ErrorBase : IPolymorphicBaseClass<ErrorBase, string> {
    private const string _discriminatorKey = "messageKey";
    /// <inheritdoc/>
    public static string DiscriminatorPropertyName => _discriminatorKey;
    /// <summary>
    /// API error message.
    /// </summary>
    [JsonPropertyName("message")] public required string Message { get; set; }
}
