using Cnml.ChurchToolsApi.Polymorphic;

namespace Cnml.ChurchToolsApi.Errors;
/// <summary>
/// Error that occurs, when a request requires authorization, but the client didn't yet log in.
/// </summary>
public class UnauthorizedError : ErrorBase, IPolymorphicDerivedClass<ErrorBase, string> {
    /// <inheritdoc/>
    public static string Discriminator => "exception.unauthorized";
}
