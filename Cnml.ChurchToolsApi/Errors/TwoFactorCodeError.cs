using Cnml.ChurchToolsApi.Polymorphic;

namespace Cnml.ChurchToolsApi.Errors;
/// <summary>
/// Error that occurrs when the TOTP code (2FA) provided after log�n is invalid.
/// </summary>
public class TwoFactorCodeError : ErrorBase, IPolymorphicDerivedClass<ErrorBase, string> {
    /// <inheritdoc/>
    public static string Discriminator => "two.factor.code.failed";
}
