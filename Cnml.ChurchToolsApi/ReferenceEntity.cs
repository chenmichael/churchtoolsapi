using Cnml.ChurchToolsApi.Model;

namespace Cnml.ChurchToolsApi;
/// <summary>
/// Provides a class to refer to an entity of type <typeparamref name="TEntity"/>.
/// This can be used to chain API calls on top of this entity using the connected <typeparamref name="TClient"/>.
/// </summary>
/// <typeparam name="TEntity">Type of the referred entity.</typeparam>
/// <typeparam name="TClient">Type of the attached client.</typeparam>
/// <param name="id"></param>
/// <param name="client"></param>
public sealed class ReferenceEntity<TEntity, TClient>(int id, TClient client)
    : IApiEntity<TEntity, TClient>
    where TEntity : IIdEntity<TEntity>
    where TClient : class {
    /// <inheritdoc/>
    public int Id => id;
    /// <inheritdoc/>
    public TClient GetClient() => client;
}
